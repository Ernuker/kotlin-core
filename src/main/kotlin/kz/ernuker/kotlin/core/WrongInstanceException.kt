package kz.ernuker.kotlin.core

import kotlin.reflect.KClass

class WrongInstanceException(
    expectedClass: KClass<*>,
    actualClass: KClass<*>
) : IllegalArgumentException(
    "Expected instance of ${expectedClass.qualifiedName}, got ${actualClass.qualifiedName}"
)