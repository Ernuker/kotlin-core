package kz.ernuker.kotlin.core

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
@Throws(IllegalArgumentException::class)
inline fun <reified T : Any> requireNonNull(value: T?): T {
    contract {
        returns() implies (value != null)
    }

    if (value == null) {
        val exception = NullInsteadOfNonNullException(T::class)
        exception.printStackTrace()
        throw exception
    } else {
        return value
    }
}

@OptIn(ExperimentalContracts::class)
@Throws(WrongInstanceException::class)
inline fun <reified EXPECTED> require(actual: Any): EXPECTED {
    contract {
        returns() implies (actual is EXPECTED)
    }

    if (actual !is EXPECTED) {
        throw WrongInstanceException(EXPECTED::class, actual::class)
    } else {
        return actual
    }
}