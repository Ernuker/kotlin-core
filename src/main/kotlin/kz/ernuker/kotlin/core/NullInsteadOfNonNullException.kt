package kz.ernuker.kotlin.core

import kotlin.reflect.KClass

class NullInsteadOfNonNullException(kotlinClass: KClass<*>) : IllegalArgumentException(
    "Expected not null instance" + kotlinClass.qualifiedName?.let { "of $it" }.orEmpty()
)