There should be this lines in the top of root build.gradle file

```groovy
buildscript {
    ext {
        kotlinVersion = 'x.y.z' // Any version of kotlin
    }
    repositories {
        jcenter()
    }
    dependencies {
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    }
}
```